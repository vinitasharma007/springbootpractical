package com.brevitaz.hibernateerexample;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.brevitaz.hibernateerexample.entity.Comment;
import com.brevitaz.hibernateerexample.entity.Gender;
import com.brevitaz.hibernateerexample.entity.Post;
import com.brevitaz.hibernateerexample.entity.Tag;
import com.brevitaz.hibernateerexample.entity.User;
import com.brevitaz.hibernateerexample.entity.UserProfile;
import com.brevitaz.hibernateerexample.repository.PostRepository;
import com.brevitaz.hibernateerexample.repository.UserRepository;

@SpringBootApplication
public class HibernateRelationMappingApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(HibernateRelationMappingApplication.class, args);
	}

	@Autowired
	private PostRepository postRepository;
	@Autowired
	private UserRepository userRepository;
	
	
	@Override
	public void run(String... args) throws Exception {
		
		Post post = new Post("one to many mapping using JPA and hibernate", "one to many mapping using JPA and hibernate", null);
		
		Comment comment1 = new Comment("Very useful");
		Comment comment2 = new Comment("informative");
		Comment comment3 = new Comment("Great post");
		
		post.getComments().add(comment1);
		post.getComments().add(comment2);
		post.getComments().add(comment3);
		
		this.postRepository.save(post);
		
		
		
		// many to many 
		Post post1 = new Post("Hibernate Many to Many Mapping Example with Spring Boot", 
				"Hibernate Many to Many Mapping Example with Spring Boot", 
				"Hibernate Many to Many Mapping Example with Spring Boot");
		
		Post post2 = new Post("Hibernate One to Many Mapping Example with Spring Boot", 
				"Hibernate One to Many Mapping Example with Spring Boot", 
				"Hibernate One to Many Mapping Example with Spring Boot");
		
		Tag springBoot = new Tag("Spring Boot");
		Tag hibernate = new Tag("Hibernate");
		
		// add tag references post
		post.getTags().add(springBoot);
		post.getTags().add(hibernate);
		
		// add post references tag
		
		springBoot.getPosts().add(post1);
		hibernate.getPosts().add(post1);
		
		springBoot.getPosts().add(post2);
		post1.getTags().add(springBoot);
		
		
		this.postRepository.save(post1);
		this.postRepository.save(post2);
		
		//One to one 
		
		// user object
				User user = new User();
				user.setName("Ramesh");
				user.setEmail("ramesh@gmail.com");
				
				UserProfile userProfile = new UserProfile();
				userProfile.setAddress("Pune");
				userProfile.setBirthOfDate(LocalDate.of(1991, 03, 24));
				userProfile.setGender(Gender.MALE);
				userProfile.setPhoneNumber("1234567899");
				
				user.setUserProfile(userProfile);
				userProfile.setUser(user);
				
				userRepository.save(user);
		
	}
}