package com.brevitaz.hibernateerexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.hibernateerexample.entity.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>{

}
