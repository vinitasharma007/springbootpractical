package com.brevitaz.hibernateerexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.hibernateerexample.entity.Tag;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long>{

}
