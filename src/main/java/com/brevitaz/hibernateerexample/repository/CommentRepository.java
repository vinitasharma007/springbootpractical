package com.brevitaz.hibernateerexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.hibernateerexample.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>{

}