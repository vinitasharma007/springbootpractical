package com.brevitaz.hibernateerexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brevitaz.hibernateerexample.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}